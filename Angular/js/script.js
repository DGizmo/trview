'use strict';

var json = [
	{
		"avatar"	: "../img/1.png",
		"name"		: "John Adams",
		"count"		: "102",
		"ideas" 	: "0",
		"following" : "50",
		"followers" : "0",
		"subscribe"	: "true"
	},{
		"avatar"	: "../img/2.png",
		"name"		: "Kevin Malcolm",
		"count"		: "1472",
		"ideas" 	: "20",
		"following" : "19",
		"followers" : "26",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/3.png",
		"name"		: "James Madison",
		"count"		: "32",
		"ideas" 	: "0",
		"following" : "2",
		"followers" : "0",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/4.png",
		"name"		: "James Monroe",
		"count"		: "3086",
		"ideas" 	: "53",
		"following" : "1059",
		"followers" : "45",
		"subscribe"	: "true"
	},{
		"avatar"	: "../img/5.png",
		"name"		: "Andrew Jackson",
		"count"		: "102",
		"ideas" 	: "0",
		"following" : "50",
		"followers" : "0",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/1.png",
		"name"		: "John Adams",
		"count"		: "102",
		"ideas" 	: "0",
		"following" : "50",
		"followers" : "0",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/2.png",
		"name"		: "Kevin Malcolm",
		"count"		: "1472",
		"ideas" 	: "20",
		"following" : "19",
		"followers" : "26",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/3.png",
		"name"		: "James Madison",
		"count"		: "32",
		"ideas" 	: "0",
		"following" : "2",
		"followers" : "0",
		"subscribe"	: "true"
	},{
		"avatar"	: "../img/4.png",
		"name"		: "James Monroe",
		"count"		: "3086",
		"ideas" 	: "53",
		"following" : "1059",
		"followers" : "45",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/5.png",
		"name"		: "Andrew Jackson",
		"count"		: "102",
		"ideas" 	: "0",
		"following" : "50",
		"followers" : "0",
		"subscribe"	: "false"
	},{
		"avatar"	: "../img/1.png",
		"name"		: "John Adams",
		"count"		: "102",
		"ideas" 	: "0",
		"following" : "50",
		"followers" : "0",
		"subscribe"	: "false"
	}
];

var APP = angular.module('app', []);
APP.controller('TableList', ['$scope', function($scope) {
	$scope.list = json;

}]);