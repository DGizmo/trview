var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCSS = require('gulp-minify-css');
var stripCssComments = require('gulp-strip-css-comments');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var size = require('gulp-filesize');
var addsrc = require('gulp-add-src');


var jsValidate = require('gulp-jsvalidate');
var clean = require('gulp-clean');
var rename = require('gulp-rename');

var autoprefixer = require('gulp-autoprefixer');

gulp.task('css', function () {
    gulp.src([
        'Angular/css/style.css'
    ])
    .pipe(stripCssComments())
    .pipe(minifyCSS(''))
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest('Angular/css/'))
});



gulp.task('js', function() {
    gulp.src([
        'bower_components/angular/angular.min.js',
        'Angular/js/script.js',
    ])
     .pipe(concat("script.min.js"))  
     .pipe(uglify())    
     .pipe(gulp.dest('Angular/js/'))
});

gulp.task('angular', ['css', 'js']);



 






// gulp.task('watch',function(){
//     gulp.watch(['../data/css/core/*.css','!../data/css/core/coreMin.css'],['css-core']);
//     gulp.watch('../data/js/core/*.js',['js-core']);
//     gulp.watch('../data/js/libs/*.js',['js-modules']);
        
// })






// gulp.task('valid', function () {
//     return gulp.src('../app/*.js')
//         .pipe(jsValidate());
// });

